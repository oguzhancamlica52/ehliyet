import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

@Injectable()
export class DatabaseProvider {

  private db: SQLiteObject;
  private isOpen: boolean;
  constructor(public sqlite: SQLite) {
    // if (!this.isOpen) {
    //   this.sqlite = new SQLite();
    //   this.sqlite.create({ name: 'data.db', location: 'default' }).then((db: SQLiteObject) => {
    //     this.db = db;
    //     db.executeSql("create table users (id int primary key identity, name nvarchar(500), surname nvarchar(500))", []);
    //     this.isOpen = true;
    //   }).catch(error => {
    //     console.log(error);
    //   })
    // }
  }
  createUser(name: string, surname: string) {
    return new Promise((resolve, reject) => {
      let sql = "insert into users (name,surname) values (?,?)";
      this.db.executeSql(sql, [name, surname]).then(data => {
        resolve(data);
      }, (error) => {
        reject(error);
      });
    })
  }
  getUserAll() {
    return new Promise((resolve, reject) => {
      this.db.executeSql("select * from users", []).then(data => {
        let arrayUsers = [];
        if (data.rows.length > 0) {
          for (let i = 0; i < data.rows.length; i++) {
            arrayUsers.push({
              id: data.rows.item(i).id,
              name: data.rows.item(i).name,
              surname: data.rows.item(i).surname,
            });
          };
        }
        resolve(arrayUsers);
      }, (error) => {
        reject(error);
      })
    })
  }
}
