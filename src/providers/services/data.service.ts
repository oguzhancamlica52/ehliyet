import { Injectable, Pipe, PipeTransform  } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Subscription } from 'rxjs';
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/observable/timer'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/take'

@Injectable()
export class DataService {
    tick = 1000;
    subscription = new Subscription();
    constructor() { }
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
    private dataOutputSource = new BehaviorSubject<any>([]);
    dataOutput = this.dataOutputSource.asObservable();

    private timerSource = new BehaviorSubject<any>([]);
    timerData = this.timerSource.asObservable();

    dataOutputs(value) {
        this.dataOutputSource.next(value);
    }
    timer(counter){
         this.subscription = Observable.timer(0, this.tick)
        .take(counter)
        .map(() => --counter)
        .subscribe(t => this.timerSource.next(t));
    }
}
@Pipe({
    name: 'formatTime'
})
export class FormatTimePipe implements PipeTransform {

    transform(value: number): string {
        const minutes: number = Math.floor(value / 60);
        return ('00' + minutes).slice(-2) + ':' + ('00' + Math.floor(value - minutes * 60)).slice(-2);
    }
}