import { Component, OnInit } from '@angular/core';
import { NavParams, AlertController, ModalController, Events } from 'ionic-angular';
import { ResultModal } from '../../app/components/result-modal/result-modal.component';
import { DataService } from '../../providers/services/data.service';
import { Subscription } from 'rxjs';

@Component({
    selector: 'solve-test',
    templateUrl: 'solve-test.html'
})

export class SolveTestPage implements OnInit {

    data: any = [];
    replyData: any = [];
    countDown;
    subscription = new Subscription();
    constructor(private params: NavParams, private modalCtrl: ModalController, private alertCtrl: AlertController, private events: Events, private dataService: DataService) {}
    ngOnInit() {
        this.dataService.timer(3000);
        this.dataService.timerData.subscribe(res => {
            this.countDown = res;
        });
        this.data = this.params.get('data');
    }
    
    showAlert(puan) {
        if (!puan)
            puan = 0;
        const alert = this.alertCtrl.create({
            title: 'Sınav Sonucu!',
            subTitle: 'Puanınız :' + puan + '',
            buttons: ['Kapat']
        });
        alert.present();
    }
    presentModal() {
        const modal = this.modalCtrl.create(ResultModal);
        modal.present();
    }
    
    getReplyData() {
        this.events.publish('getData', true);

        this.finish();
        this.dataService.ngOnDestroy();
    }

    finish() {
        this.dataService.dataOutput.subscribe(res => {
            this.replyData = res;
        });
        var i = 0;
        var result = 0;
        this.replyData.forEach(e => {
            if (e.selected == 1 && e.trueReply == "A")
                i++;
            if (e.selected == 2 && e.trueReply == "B")
                i++;
            if (e.selected == 3 && e.trueReply == "C")
                i++;
            if (e.selected == 4 && e.trueReply == "D")
                i++;
        });
        result = (i * 100) / this.data.length;
        this.showAlert(result);
    }
}
