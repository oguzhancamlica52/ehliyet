import { Component, Injectable, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SolveTestPage } from '../solve-test/solve-test';
import { RandomTestPage } from '../random-test/random-test';
import { DatabaseProvider } from '../../providers/database/database';
import { DataService } from '../../providers/services/data.service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
@Injectable()
export class HomePage implements OnInit {
  examples = [
    {
      id: 1,
      img: 'soru1.PNG',
      trueReply: 'D'
    }, {
      id: 2,
      img: 'soru2.PNG',
      trueReply: 'A'
    }, {
      id: 3,
      img: 'soru3.PNG',
      trueReply: 'C'
    }, {
      id: 4,
      img: 'soru4.PNG',
      trueReply: 'B'
    }, {
      id: 5,
      img: 'soru5.PNG',
      trueReply: 'C'
    },
  ]
  constructor(public navCtrl: NavController, private database: DatabaseProvider, private dataService: DataService) {
    // this.createUser();
  }
  ngOnInit() {
    this.dataService.ngOnDestroy();
  }
  // createUser() {
  //   this.database.createUser("oğuzhan", "çamlıca").then(data => {
  //     console.log(data);
  //   }, (error) => {
  //     console.log(error);
  //   });
  //   this.getAllUser()
  // }
  // getAllUser() {
  //   this.database.getUserAll().then(data => {
  //     console.log(data);
  //   }, error => {
  //     console.log(error);
  //   })
  // }
  solveTest() {
    this.navCtrl.setRoot(SolveTestPage, { text: 'Sınav Soruları', data: this.examples });
  }
  randomTest() {
    this.navCtrl.setRoot(RandomTestPage, { text: 'Karışık Sorular' })
  }
}
