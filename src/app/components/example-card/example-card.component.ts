import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Events } from 'ionic-angular';
import { DataService } from '../../../providers/services/data.service';

@Component({
    selector: 'example-card',
    templateUrl: 'example-card.component.html'
})

export class ExampleCardComponent implements OnInit {
    @Input() data: any = [];

    selected: string = "";

    replyButton = [
        {
            id: 1,
            name: 'A'
        }, {
            id: 2,
            name: 'B'
        }, {
            id: 3,
            name: 'C'
        }, {
            id: 4,
            name: 'D'
        }
    ];

    constructor(private events: Events, private dataService:DataService) {

    }

    ngOnInit() { 
            this.events.subscribe('getData', (res) => {
                if (res)
                    this.dataService.dataOutputs(this.data);
            });
    }
}