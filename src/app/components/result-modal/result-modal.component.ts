import { Component, OnInit } from '@angular/core';
import { ViewController } from 'ionic-angular';

@Component({
    selector: 'result-modal',
    templateUrl: 'result-modal.component.html'
})

export class ResultModal implements OnInit {
    constructor(private viewCtrl:ViewController) { }

    ngOnInit() { }
    dismiss() {
        this.viewCtrl.dismiss();
      }
}