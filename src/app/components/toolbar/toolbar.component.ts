import { Component, OnInit, Input } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../../../pages/home/home';

@Component({
    selector: 'toolbar',
    templateUrl: 'toolbar.component.html'
})

export class ToolbarComponent implements OnInit {
    constructor(private navCtrl: NavController, private params:NavParams) { }
    text:string;
    bgColor:String;
    ngOnInit() { 
       this.text = this.params.get('text');
    }
    back() {
        this.navCtrl.setRoot(HomePage);
    }
}