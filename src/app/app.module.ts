import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SolveTestPage } from '../pages/solve-test/solve-test';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { RandomTestPage } from '../pages/random-test/random-test';
import { SQLite } from '@ionic-native/sqlite';
import { DatabaseProvider } from '../providers/database/database';
import { HttpModule } from '@angular/http';
import { ExampleCardComponent } from './components/example-card/example-card.component';
import { ResultModal } from './components/result-modal/result-modal.component';
import { DataService, FormatTimePipe } from '../providers/services/data.service';
@NgModule({
  declarations: [
    FormatTimePipe,
    MyApp,
    HomePage,
    SolveTestPage,
    ToolbarComponent,
    RandomTestPage,
    ExampleCardComponent,
    ResultModal
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SolveTestPage,
    RandomTestPage,
    ResultModal
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    SQLite,
    DatabaseProvider,
    DataService
  ]
})
export class AppModule { }
